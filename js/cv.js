var imgs=[];
var images=document.querySelectorAll(".menu-img");
var menu = document.querySelector(".lightBox");
var close = document.getElementById("close");
var menuImg =document.querySelector(".lightBox-img");
var imgIndex = 0;
var next = document.getElementById("next");
var prev = document.getElementById("prev");

/*

for(var i=0 ; i<images.length ; i++)
    {
        imgs.push(images[i]);
        images[i].addEventListener("click",function(e){
            
            menu.style.display="block";
            
var imgSrc = e.target.src;
            
            menuImg.style.backgroundImage = "url("+imgSrc+")";
            imgIndex = imgs.indexOf(e.target);
    
            
        })
    }
close.addEventListener("click",function(){
    menu.style.display="none";
})
document.addEventListener("keydown",function(e){
    if(e.keyCode == 27)
        {
          menu.style.display="none";  
        }
    
    
})
next.addEventListener("click",function(){
    imgIndex++;
    if(imgIndex == imgs.length){
        imgIndex = 0;
    }
   var currentIndex = imgs[imgIndex].src;
    menuImg.style.backgroundImage = "url("+currentIndex+")";
})
document.addEventListener("keydown",function(e){
    if(e.keyCode == 39)
        {
          imgIndex++;
    if(imgIndex == imgs.length){
        imgIndex = 0;
    }
   var currentIndex = imgs[imgIndex].src;
    menuImg.style.backgroundImage = "url("+currentIndex+")";
        }
    
    
})
prev.addEventListener("click",function(){
    imgIndex--;
    if(imgIndex <  0){
        imgIndex = imgs.length-1;
    }
  var  currentIndex = imgs[imgIndex].src;
    menuImg.style.backgroundImage = "url("+currentIndex+")";
})
document.addEventListener("keydown",function(e){
    if(e.keyCode == 37)
        {
           imgIndex--;
    if(imgIndex <  0){
        imgIndex = imgs.length-1;
    }
  var  currentIndex = imgs[imgIndex].src;
    menuImg.style.backgroundImage = "url("+currentIndex+")"; 
        }
    
    
})
*/


function smothScroll(target,duration){
    
    var target = document.querySelector(target);
    var targetPosition = target.getBoundingClientRect().top;
    var startPosition = window.pageYOffset;
    var distance = targetPosition - startPosition;
    var startTime = null ;
  
    
    function animation(currentTime){
    
    if(startTime === null) {startTime = currentTime};  
    var elapsedTime = currentTime - startTime ;  
    var run = ease(elapsedTime,startPosition,distance,duration);  
    window.scrollTo(0,run);
    if(elapsedTime < duration){requestAnimationFrame(animation)};  
  }
    function ease(t,b,c,d){
        t /= d / 2;
        if (t < 1) return c / 2 * t * t +b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
    }
    
    requestAnimationFrame(animation);
}

var scRoll6 = document.querySelector(".scroll-1")
var scRoll1 = document.querySelector(".scroll-2");
var scRoll2 = document.querySelector(".scroll-3");
var scRoll3 = document.querySelector(".scroll-4");
var scRoll4 = document.querySelector(".scroll-5");
var scRoll5 = document.querySelector(".scroll-6");

scRoll6.addEventListener("click",function(){
    
    smothScroll("#home",700)
    
})
scRoll1.addEventListener("click",function(){
    
    smothScroll("#mySkill",1000)
    
})
scRoll2.addEventListener("click",function(){
    
    smothScroll("#myWork",1300)
    
})
scRoll3.addEventListener("click",function(){
    
    smothScroll("#portfolio",1600)
    
})
scRoll4.addEventListener("click",function(){
    
    smothScroll("#experience",1900)
    
})

scRoll5.addEventListener("click",function(){
    
    smothScroll("#contact",2200)
    
})
